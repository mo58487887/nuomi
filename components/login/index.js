const md5 = require('../../utils/md5')
import { isPhone } from "../../utils/util";
import ajax from '../../utils/request'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    mobile: '',
    smsCode: '',
    password: '',
    timeDownSeconds: 0,
    choose: true,
    code: '',
    authWrapMarginTop: 350
  },
  lifetimes:{
    created() {
      wx.login({
        success: ({code}) => {
          if (code) {
            this.setData({
              code
            })
          }
        }
      })
    }
  },
  
  /**
   * 组件的方法列表
   */
  methods: {
    onLoad() {
      this.setData({
        authWrapMarginTop: 450
      })
    },
    onChangeChoose() {
      this.setData({
        choose: false
      })
    },
    onGetPhoneNumber(e) {
      const {encryptedData, iv} = e.detail
      if (encryptedData && iv) {
        wx.checkSession({
          success: () => {
            this.wxGetPhoneNumber(this.data.code, iv, encryptedData)
          },
          fail: () => {
            wx.login({
              success: (res) => {
                const {code} = res
                this.setData({
                  code
                }, () => {
                  this.wxGetPhoneNumber(this.data.code, iv, encryptedData)
                })
              }
            })
          }
        })
      }
    },

    wxGetPhoneNumber(code, iv, encryptedData) {
      ajax({
        url: '/wxMini/getPhoneNumber',
        data: {code, iv, encryptedData}
      }).then(({code, data}) => {
        if (code === 0 && data && data.phone) {[
          this.setData({
            mobile: data.phone,
            choose: false
          }, this.onGetSmsCode)
        ]}
      })
    },

    onInputChange({detail: {value}, currentTarget: {dataset: {type}}}) {
      this.setData({
        [type]: value
      })
    },
    // 获取验证码
    onGetSmsCode() {
      const {mobile} = this.data
      if (!isPhone(mobile)) {
        wx.showToast({
          title: '请输入正确的手机号',
          icon: 'none'
        })
        return
      }
      ajax({
        url: '/user/generateVerificationCode',
        data: {mobile, statusCode: 2}
      }).then(({code, body}) => {
        if (code === 0) {
          wx.showToast({
            title: '短信验证码已发送，请注意查看手机',
            icon: 'none'
          })
          this.onTimeDown(60)
        }
      })
      // 
    },
    // 直接登录
    onDirectLogin() {
      const {mobile, smsCode, inviteCode} = this.data
      if (!isPhone(mobile)) {
        wx.showToast({
          title: '请检查手机号',
          icon: 'none'
        })
        return
      }
      if (!smsCode) {
        wx.showToast({
          title: '请输入短信验证码',
          icon: 'none'
        })
        return
      }
      if (inviteCode && !isPhone(inviteCode)) {
        wx.showToast({
          title: '推荐人手机号格式不正确',
          icon: 'none'
        })
        return
      }
      wx.login({
        success: ({code}) => {
          if (code) {
            ajax({
              url: '/wxMini/accountVftLogin',
              data: {mobile, codeText: smsCode, code, inviteCode: ''}
            }).then(({code, data, message}) => {
              if (code === 0) {
                const app = getApp()
                app.globalData.userInfo = data
                wx.setStorageSync('userInfo', data)
                if (this.triggerEvent) {
                  this.triggerEvent('onlogin', {data})
                }
              } else {
                wx.showToast({
                  title: message,
                  icon: 'none'
                })
              }
            })
          }
        }
      })
      // ajax({
      //   url: '/user/generateRandomCodes'
      // }).then(({code, data}) => {
      //   if (code === 0) {
      //     const {randomCodes} = data
      //     const codeText = md5(md5(smsCode) + randomCodes)
      //     return 
      //   }
      // })
    },
    onTimeDown(t = this.data.timeDownSeconds) {
      if (t === 0) {
        return
      } else {
        setTimeout(() => {
          this.setData({
            timeDownSeconds: t - 1
          }, this.onTimeDown)
        }, 1000)
      }
    }
  }
})
