
Component({
  
  externalClasses: ['my-class'],

  properties: {
    cover: String,
    title: String,
    money: null,
    otherId: null,
    goodsId: null,
    goodsType: {
      type: null,
      value: '0'
    },
    desc: String,
    killMoney: Number,
    oldMoney: Number,
  },


  data: {
    errorCover: ''
  },


  methods: {
    bindError() {
      this.setData({
        errorCover: '/asset/error.jpg'
      })
    }
  }
})
