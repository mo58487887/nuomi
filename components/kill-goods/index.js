
Component({

  options: {
    multipleSlots: true 
  },
  externalClasses: ['my-class'],
  properties: {
    otherId: Number,
    cover: String,
    buyText: String,
    desc: String,
    title: String,
    killMoney: Number,
    oldMoney: Number,
    goodsId: null,
    goodsType: {
      type: null,
      value: '2'
    }
  },

  lifetimes: {
    created() {
      
    }
  },

  data: {
    errorCover: ''
  },

  methods: {
    bindError() {
      this.setData({
        errorCover: '/asset/error.jpg'
      })
    }
  }
})
