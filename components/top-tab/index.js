
Component({

  externalClasses: ['my-class'],

  properties: {
    current: {
      type: null,
      observer(n) {
        this.setData({
          currentType: n
        })
      }
    },
    typeNav: Array
  },


  data: {
    currentType: ''
  },

  lifetimes: {
    attached() {
      const {current, typeNav} = this.data
      if (current || current === 0) {
        this.setData({
          currentType: current
        })
      } else {
        this.setData({
          currentType: typeNav[0].type
        })
      }
    }
  },

  methods: {
    onTypeNavTap({currentTarget: {dataset: {type}}}) {
      this.setData({
        currentType: type
      })
      this.triggerEvent('onchange', {type})
    }
  }
})
