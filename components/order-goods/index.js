// components/order-goods/index.js
Component({
  externalClasses: ['my-class'],

  properties: {
    cover: String,
    title: String,
    spec: String,
    num: null,
    money: null
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
