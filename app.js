import ajax from "./utils/request";

App({
  onLaunch() {
    wx.login({
      success: ({code}) => {
        ajax({
          url: '/wxMini/wxLogin',
          data: {code}
        }).then(({code, data}) => {
          if (code === 0) {
            this.globalData.userInfo = data
            wx.setStorageSync('userInfo', data)
            const pages = getCurrentPages()
            if (pages.length > 0) {
              const current = pages[pages.length - 1]
              if (current.route === 'pages/mine/index') {
                current.setData({
                  showView: 1,
                  userInfo: data
                })
              }
            }
          }
        })
      }
    })
  },
  globalData: {
    userInfo: null
  }
})