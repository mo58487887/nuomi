// 正式服务器
// const baseURL = 'http://www.nuomibaobei.com'
// 测试
const baseURL = 'http://39.96.71.80'
// 张硕
// const baseURL = "http://192.168.0.45:8080"
// 张峰
// const baseURL = "http://192.168.0.36:8080"

function ajax(config) {
  const method = config.method || 'POST'
  const header = {}
  const url = baseURL + config.url
  const data = config.data || {}
  if (method == 'POST') {
    header['content-type'] = "application/x-www-form-urlencoded"
  }
  const JSESSIONID = wx.getStorageSync('JSESSIONID')
  if (JSESSIONID) header['Cookie'] = `JSESSIONID=${JSESSIONID}`
  return new Promise((resolve, reject) => {
    wx.request({
      url,
      data,
      method,
      header,
      success: ({header, data}) => {
        if (header['Set-Cookie']) {
          setSession(header['Set-Cookie'])
        }
        if (data.code === 0) {

        } else {

        }
        resolve(data)
      },
      fail: function () {
        reject('网络连接失败')
      }
    })
  })
  .catch(res => {
    if (typeof res === 'object') {
      wx.showToast({
        title: res.message,
        icon: 'none'
      })
      return res
    } else if (typeof res === 'string') {
      wx.showToast({
        title: res,
        icon: 'none'
      })
      return Promise.reject()
    }
  })
}

function setNetCache() {
}

function getNetCache(url) {
  const value = wx.getStorageSync(url)
  if (value) {
    return value
  } else return null
}
function setSession(cookie) {
  const sessionStr = cookie.split(';').find(_ => /^JSESSIONID/.test(_))
  const sessionId = sessionStr.substring(sessionStr.indexOf('=') + 1)
  wx.setStorageSync('JSESSIONID', sessionId)
}
export default ajax