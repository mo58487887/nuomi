import ajax from './request'
export function isPhone(str) {
  const myreg = /^[1][3,4,5,6,7,8][0-9]{9}$/;
  if (!myreg.test(str)) {
    return false;
  } else {
    return true;
  }
}
/**
 * 
 * @param {(number | string)} w 占位宽度
 * @param {(number | string)=} h 占位高度
 */
export const placeImage = (w, h = w) => `https://tuimeizi.cn/random?w=${w}&h=${h}&s=0`
/**
 * 
 * @param {number} rpx rpx
 * @returns {number} 返回px
 */
export function rpx2px(rpx) {
  const {windowWidth} = wx.getSystemInfoSync();
  return rpx / 750 * windowWidth; 
}
/**
 * 
 * @param {number} px px
 * @returns {number} 返回rpx
 */
export function px2rpx(px) {
  const {windowWidth} = wx.getSystemInfoSync();
  return px * 750 / windowWidth; 
}
/**
 * 
 * @param {!Object} config dataKeyName、localKeyName、url、data
 */
export function getList(config) {
  const {dataKeyName, localKeyName, url} = config
  const pageNum = config.pageNum || this.data[localKeyName].pageNum
  const pageSize = config.pageSize || 10
  const data = config.data || {}
  const {list, hasNext} = this.data[localKeyName]
  if (!hasNext) {
    return
  }
  ajax({
    url,
    data: Object.assign({}, {page: pageNum, rows: pageSize, row: pageSize}, data)
  }).then(({code, data}) => {
    if (code !== 0 || !data) {
      return
    }
    const dotIndex = dataKeyName.indexOf('.')
    const listData = dotIndex > -1 ? data[dataKeyName.substring(0, dotIndex)][dataKeyName.substring(dotIndex + 1)] : data[dataKeyName]
    if (code === 0 && listData && Array.isArray(listData) && listData.length > 0) {
      this.setData({
        [`${localKeyName}.list`]: list.concat(listData),
        [`${localKeyName}.pageNum`]: pageNum + 1
      }, () => {
        if (config.callback) {
          config.callback()
        }
      })
    } else {
      this.setData({
        [`${localKeyName}.hasNext`]: false
      })
    }
  })
}
/**
 *
 * @param fn {Function}   实际要执行的函数
 * @param delay {Number}  延迟时间，单位是毫秒（ms）
 *
 * @return {Function}     返回一个“防反跳”了的函数
 */

export function debounce(fn, delay) {

  // 定时器，用来 setTimeout
  var timer

  // 返回一个函数，这个函数会在一个时间区间结束后的 delay 毫秒时执行 fn 函数
  return function () {

    // 保存函数调用时的上下文和参数，传递给 fn
    var context = this
    var args = arguments

    // 每次这个返回的函数被调用，就清除定时器，以保证不执行 fn
    clearTimeout(timer)

    // 当返回的函数被最后一次调用后（也就是用户停止了某个连续的操作），
    // 再过 delay 毫秒就执行 fn
    timer = setTimeout(function () {
      fn.apply(context, args)
    }, delay)
  }
}