import { rpx2px } from "../../../../utils/util";

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  lifetimes: {
    created() {
    }
  },

  data: {

  },

  
  methods: {
    onClose() {
      this.triggerEvent('onclose')
    }
  }
})
