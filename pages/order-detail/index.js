// pages/order-detail/index.js
Page({

  data: {
    expressModalShow: false
  },

  onLoad(options) {

  },
  
  onShow() {

  },
  onOpenModal() {
    this.setData({
      expressModalShow: true
    })
  },
  onCloseModal() {
    this.setData({
      expressModalShow: false
    })
  }
})