import { getList } from "../../utils/util";
import ajax from "../../utils/request";

const typeNav = [
  {
    type: 1,
    text: '未使用',
  },
  {
    type: 2,
    text: '已使用',
  },
  {
    type: 3,
    text: '已过期',
  },
]
const app = getApp()
Page({

  data: {
    couponList: {
      list: [1,2,3,4],
      hasNext: true,
      pageNum: 1,
      pageSize: 10
    },
    typeNav,
    currentType: 1
  },
  
  onLoad(options) {
    this.onTypeNavTap({detail: {type: 1}})
    const userInfo = wx.getStorageSync('userInfo')
  },
  onTypeNavTap({detail: {type}}) {
    this.setData({
      currentType: type,
      couponList: {
        list: [],
        hasNext: true,
        pageNum: 1,
        pageSize: 10
      }
    }, this.getCouponList)
  },
  getCouponList() {
    // usageState
    const userInfo = wx.getStorageSync('userInfo')
    getList.bind(this, {
      localKeyName: 'couponList',
      dataKeyName: 'userCoupons',
      url: '/coupon/selectByUsageState',
      data: {usageState:this.data.currentType, userId: userInfo.userId}
    })()
  }
})