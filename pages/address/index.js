import ajax from '../../utils/request'
Page({

  data: {
    addressList: []
  },

  onLoad(options) {

  },

  onShow() {
    const {userId} = wx.getStorageSync('userInfo')
    ajax({
      url: '/consignee/getByUser',
      data: {userId}
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          addressList: data.result
        })
      }
    })
  },
  onAddressItemTap({currentTarget: {dataset: {id}}}) {
    wx.navigateTo({
      url: '/pages/add-address/index?addressId=' + id
    })
  },
  onSetConsigneeState({currentTarget: {dataset: {id}}}) {
    const {addressList} = this.data

  },
  onRemoveAddressItem({currentTarget: {dataset: {id}}}) {
    const {addressList} = this.data
    
  }
})