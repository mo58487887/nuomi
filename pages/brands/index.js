import { getList } from "../../utils/util";

Page({

  data: {
    brands: {
      pageNum: 1,
      pageSize: 20,
      hasNext: true,
      list: []
    }
  },

  onLoad(options) {
    this.getList()
  },
  getList() {
    const foo = getList.bind(this, {
      localKeyName: 'brands',
      dataKeyName: 'list',
      url: '/brand/queryBrandCategoryList'
    })
    foo()
  }
})