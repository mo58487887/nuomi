import { placeImage, px2rpx } from "../../utils/util";
import ajax from "../../utils/request";

Page({

  data: {
    info: {}, //商品详情
    chooseNum: 1, //选择的数量,
    chooseModalShow: false, // 规格选择弹起
    startY: 0, // ???
    scrollHeight: 0, // ??? 
    detailImages: null,
    goodsSpec: ['', ''],
    currentSpec: [],
    currentSpecInfo: '',
    goodsType: '2',
    downCount: { // 倒计时
      hours: '00',
      minutes: '00',
      seconds: '00'
    },
    pageShow: false,
    isAddingToCar: false
  },
  onLoad(options) {
  },
  onShow() {
    const {id, goodsId, goodsType} = this.options
    this.setData({
      goodsType, pageShow: true
    })
    ajax({
      url: '/goods/getGoodsByGoodsId' ,// '/goods/getGoodsByGoodsIds',
      data: {id, goodsId, goodsType}
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          info: data.result.goodsHead,
          detailImages: data.result.goodsImg
        }, () => {
          if (goodsType !== 2) {
            this.countDown(undefined, true)
          }
        })
      }
      // 规格
      if (goodsType != '2') {
        return ajax({
          url: '/goods/getKillGoodsAttrByKillId',
          data: {
            killId: data.result.goodsHead.sid,
            goodsId: data.result.goodsHead.id,
            sufflierGoodsId: data.result.goodsHead.goodsId
          }
        })
      } else {
        return ajax({
          url: '/goods/getGoodsSpecByGoodsId',
          data: {goodsId, id}
        })
      }
    }).then(({code, data}) => {
      if (code === 0 && data && data.result && data.result.spec) {
        const spec = data.result.spec[0]
        const goodsSpec = {}
        goodsSpec.origin = data
        goodsSpec.spec = [
          {label: '规格', value: spec.name.split(',')},
          {label: '数量', value: spec.value.split(',')},
        ]
        this.setData({
          goodsSpec,
          currentSpec: [goodsSpec.spec[0].value[0], goodsSpec.spec[1].value[0]]
        }, this.getGoodsSpecItem)
      }
    })
  },
  // 倒计时
  countDown(t = this.data.info.timeStamp, show = this.data.pageShow) {
    if (t > 0 && show) {
      const nextT = t - 1000
      const days = parseInt(nextT/3600/1000/24)
      const hours = parseInt((nextT - days * 3600 * 1000 * 24)/3600/1000)
      const minutes = parseInt((nextT - days * 3600 * 1000 * 24 - hours * 3600 * 1000) / 60 / 1000)
      const seconds = parseInt((nextT - days * 3600 * 1000 * 24 - hours * 3600 * 1000 - minutes * 60 * 1000) / 1000 )
      this.setData({
        downCount: {
          days, 
          hours: hours > 9 ? hours : `0${hours}`, 
          minutes: minutes > 9 ? minutes : `0${minutes}`, 
          seconds: seconds > 9 ? seconds : `0${seconds}`
        },
        'info.timeStamp': nextT
      }, () => {
        setTimeout(this.countDown, 1000)
      })
    } 
    else if (t <= 0 && show) {
      wx.showToast({
        title: '秒杀已结束',
        icon: 'none'
      })
      // wx.switchTab({
      //   url: '/pages/index/index'
      // })
    }
  },
  // 获取规格库存
  getGoodsSpecItem(spec0 = this.data.currentSpec[0], spec1 = this.data.currentSpec[1]) {
    const {info, goodsSpec: {origin}} = this.data
    if (info.supplier === "houniao") {
      const data = JSON.stringify({
        sfgoodsId: origin.result.spec[0].supplierGoodsId, 
        goodsId: info.id, 
        attrParentName: spec0,
        attrNum: spec1
      })
      ajax({
        url: '/goods/getGoodsKucun',
        data: {data, supplier: info.supplier, isword: ''}
      }).then(({code, data}) => {
        if (code === 0 && data && data.result && data.result.attr) {
          if (JSON.stringify(data.result.attr) !== "{}") {
            this.setData({
              currentSpecInfo: data.result.attr,
            })
          } else {
            wx.showToast({
              title: '无该商品规格',
              icon: 'none'
            })
          }
        }
      })
    } else if (info.supplier === 'openapi') {

    }
  },
  // 封面图片错误处理
  bindImageError({currentTarget: {dataset: {idx}}}) {
    const {info: {goodsImg}} = this.data
    const goodsImgArr = goodsImg.split(',')
    goodsImgArr[idx] = '/asset/error.jpg'
    this.setData({
      'info.goodsImg': goodsImgArr.join(',')
    })
  },
  // 分享
  onShareAppMessage() {
    const {info: {goodsName, goodsImg}} = this.data
    return {
      title: goodsName,
      imageUrl: goodsImg.split(',')[0]
    }
  },
  // 设置选择的商品数量
  onSetChooseNum({detail: {value}}) {
    const {chooseNum, currentSpecInfo: {stockNum}} = this.data
    const currentSpecNum = stockNum || 0
    const num = parseInt(value)
    if (value.indexOf('.') > -1) {
      wx.showToast({
        title: '请输入大于0的正确整数',
        icon: 'none'
      })
      this.setData({
        chooseNum
      })
      return
    }
    if (num > 0 && num <= currentSpecNum) {
      this.setData({
        chooseNum: parseInt(value)
      })
    } else if (num > 0 && num > currentSpecNum) {
      wx.showToast({
        title: '所选数量不能大于库存数量',
        icon: 'none'
      })
      this.setData({
        chooseNum
      })
    } else {
      wx.showToast({
        title: '请输入大于0的正确整数',
        icon: 'none'
      })
      this.setData({
        chooseNum
      })
    }
  },
  // 加入购物车按钮
  onAddCarTap() {
    const {currentSpecInfo: {stockNum}, currentSpec, isAddingToCar} = this.data
    const currentSpecNum = stockNum || 0
    if (currentSpecNum <= 0 || !isAddingToCar) {
      this.setData({
        chooseModalShow: true,
        isAddingToCar: true
      })
    } else {
      this.handleAddToCar()
    }
  },
  // 加入购物车数据交互
  handleAddToCar() {
    const {currentSpecInfo, info: {id, supplier, isword}, chooseNum, goodsType} = this.data
    if (supplier === 'houniao') {
      const sendData = {
        attrId: currentSpecInfo.id,
        goodsId: id,
        goodsNum: chooseNum,
        limitNumber: 0,
        goodsType,
        attr: [],
        userId: wx.getStorageSync('userInfo').userId
      }
      ajax({
        url: '/shoppingCar/addShoppingCar',
        data: {
          supplier, isword, data: JSON.stringify(sendData)
        }
      }).then(({code, data}) => {
        if (code === 0) {
          wx.showToast({
            icon: 'none',
            title: '购物车添加成功！'
          })
          this.setData({
            chooseModalShow: false,
            isAddingToCar: false
          })
        }
      })
    }
  },
  // 打开选择规格弹窗
  onOpenChooseModal() {
    this.setData({
      chooseModalShow: true
    })
  },
  // 关闭规格选择
  onCloseSpecChoose() {
    this.setData({
      chooseModalShow: false,
      isAddingToCar: false
    })
  },
  // 防止滚动
  catchChooseModalMove() {},
  // 选择规格
  onChooseSpecItem({currentTarget: {dataset: {index, value}}}) {
    this.setData({
      [`currentSpec[${index}]`]: value
    }, this.getGoodsSpecItem)
  },
  // 确定选择规格
  onChooseSpec() {
    const {currentSpecInfo: {stockNum}, chooseNum, isAddingToCar} = this.data
    const currentSpecNum = stockNum || 0
    if (currentSpecNum > 0 && chooseNum > 0 && chooseNum <= currentSpecNum) {
      if (isAddingToCar) {
        this.handleAddToCar()
      } else {
        this.setData({
          chooseModalShow: false
        })
      }
    } else {
      wx.showToast({
        title: '该库存为0或该规格不存在，请重新选择',
        icon: 'none'
      })
    }
  },
  onCoverPreview() {
    const {goodsImg} = this.data.info
    wx.previewImage({
      urls: goodsImg.split(','),
      current: goodsImg.split(',')[0]
    })
  },
  onLessChooseNum() {
    if (this.data.chooseNum === 1) {
      return
    }
    this.onSetChooseNum({detail: {value: `${ this.data.chooseNum - 1}`}})
  },
  onMoreChooseNum() {
    this.onSetChooseNum({detail: {value: `${this.data.chooseNum + 1}`}})
  },
})