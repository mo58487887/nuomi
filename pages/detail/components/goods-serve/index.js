// pages/detail/components/goods-serve/index.js
Component({
  externalClasses: ['my-class'],

  properties: {
    isWorld: {
      type: null,
      observer(n) {
        const goodsOtherServe = [
          {
            text: '包邮',
            isShow: true
          },
          {
            text: '7天无理由退货',
            isShow: n != "1" 
          },
          {
            text: '假一赔十',
            isShow: true
          },
        ]
        this.setData({
          goodsOtherServe
        })
      }
    },
    chooseSpec: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    goodsOtherServe: []
  },

  lifetimes: {
    attached() {
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    onChooseSpec() {
      this.triggerEvent('onchoosespec')
    }
  }
})
