import { getList } from "../../utils/util";
import ajax from "../../utils/request";

Page({

  data: {
    goodsList: {
      list: [],
      pageNum: 1,
      pageSize: 10,
      hasNext: true
    }
  },
  onLoad (options) {
    this.getList()
  },
  getList() {
    const foo = getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: '',
      url: '/abroad/queryAbroadShopGoodsList'
    })
    foo()
  }
})