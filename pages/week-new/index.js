import { getList } from "../../utils/util";

// pages/week-new/index.js
Page({

  data: {
    goodsList: {
      list: [],
      pageNum: 1,
      pageSize: 10,
      hasNext: true
    }
  },

  onLoad(options) {
    this.getList()
  },
  
  getList() {
    getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: 'result.list',
      url: '/goods/queryNewGoodsListByIsNewGoods'
    })()
  }
})