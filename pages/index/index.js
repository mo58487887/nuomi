import ajax from "../../utils/request";
import { getList } from "../../utils/util";
const indexMenu = [
  {
    image: '/asset/fuli.png',
    title: '每日福利',
    path: ''
  },
  {
    image: '/asset/pinpai.png',
    title: '品牌推荐',
    path: '/pages/brands/index'
  },
  {
    image: '/asset/shangxin.png',
    title: '每周上新',
    path: '/pages/week-new/index'
  },
  {
    image: '/asset/kefu.png',
    title: '客服',
    path: ''
  },
]

Page({
  data: {
    indexMenu,
    headerImage: [],
    guessList: {
      hasNext: true,
      list: [],
      pageNum: 1,
      pageSize: 10
    },
    downCount: {
      seconds: '00',
      minutes: '00',
      hours: '00'
    },
    secondsKill: {
      downCount: 0,
      siteNum: '00'
    },
    sug: [],
    pageShow: false
  },
  
  onLoad() {
    ajax({
      url: '/index/queryHeaderImage'
    }).then(res => {
      if (res.code === 0) {
        this.setData({
          headerImage: res.data.list
        })
      }
    })
    ajax({
      url: '/index/queryIndexImageAreaList'
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          sug: data.img
        })
      }
    })
    this.getList()
  },
  getList() {
    const guessConfig = {
      localKeyName: 'guessList',
      dataKeyName: 'result.list',
      url: '/goods/queryBoutiqueGoods'
    }
    const foo = getList.bind(this, guessConfig)
    foo()
  },
  onShow() {
    this.setData({
      pageShow: true
    }, this.getSecondKillGoods)
  },
  onHide() {
    this.setData({
      pageShow: false
    })
  },
  onUnload() {
    this.setData({
      pageShow: false
    })
  },
  onReachBottom() {
    this.getList()
  },
  countDown(t = this.data.secondsKill.downCount) {
    if (t > 0 && this.data.pageShow) {
      const nextT = t - 1000
      const hours = parseInt(nextT/3600/1000)
      const minutes = parseInt((nextT - hours * 3600 * 1000) / 60 / 1000)
      const seconds = parseInt((nextT - hours * 3600 * 1000 - minutes * 60 * 1000) / 1000 )
      this.setData({
        downCount: {
          hours: hours > 9 ? hours : `0${hours}`, 
          minutes: minutes > 9 ? minutes : `0${minutes}`, 
          seconds: seconds > 9 ? seconds : `0${seconds}`
        },
        'secondsKill.downCount': nextT
      }, () => {
        setTimeout(this.countDown, 1000)
      })
    } 
    else if (t <= 0 && this.data.pageShow) {
      this.getSecondKillGoods()
    }
  },
  getSecondKillGoods() {
    ajax({
      url: '/index/querySecondsKill'
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          secondsKill: Object.assign({}, data, {siteNum: parseInt(data.siteNum)})
        }, () => {
          if (data.code != 3) {
            this.countDown()
          }
        })
      } else {
        this.setData({
          secondsKill: null
        })
      }
    })
  }
})
