import ajax from "../../utils/request";
import { getList, debounce } from "../../utils/util";
Page({

  data: {
    searchKey: '', // 输入字段
    history: [],  // 历史记录
    hotSearch: [],  // 热门搜索
    listShow: false, // 列表展示状态还是搜索输入状态
    goodsList: { // 列表 数据以及控制
      list: [],
      hasNext: true,
      pageNum: 1,
      pageSize: 10
    },
    squareShow: false, // 列表展现方式
    goodsType: 'search', // 类型，分类还是搜索
    inputFocusStatus: false, // 键盘聚焦,
    screenTop: 0, //页面位置
    
  },

  onLoad(options) {
    const history = wx.getStorageSync('searchHistory')
    const {type} = options
    if (history) {
      this.setData({
        history: JSON.parse(history)
      })
    }
    if (type && type === 'cate') {
      this.setData({
        listShow: true,
        goodsType: 'cate'
      })
      this.getCateGoodsList()
    } else if (type && type === 'brand') {
      this.setData({
        listShow: true,
        goodsType: 'cate'
      })
      this.getBrandGoodsList()
    } else {
      this.setData({
        inputFocusStatus: true
      })
    }

  },
  onReachBottom() {
    const {goodsType} = this.data
    if (goodsType === 'cate') {
      this.getCateGoodsList()
    } else if (goodsType === 'search') {
      this.getSearchGoodsList({
        goodsName: this.data.searchKey
      })
    }
  },
  // 下面是关于搜索部分的函数
  onSetHistory() {
    // 设置搜索历史
    // 存储不大于10，且不能存在相同的值
    const {history, searchKey} = this.data
    if (!searchKey) return
    const index = history.findIndex(_ => _ === searchKey)
    let newHistory
    if (index > -1) {
      newHistory = [searchKey].concat(history.filter(_ => _ !== searchKey))
    } else  {
      if (history.length < 10) {
        newHistory = [searchKey].concat(history)
      } else {
        newHistory = [searchKey].concat(history.filter((_, i) => i < 9))
      }
    }
    this.setData({
      history: newHistory
    })
    wx.setStorageSync('searchHistory', JSON.stringify(newHistory))
  },
  onSearchTap() {
    // 搜索和取消按钮
    const {searchKey, goodsList: {list, hasNext}} = this.data
    if (!searchKey && list.length === 0) {
      wx.navigateBack({delta: 1})
    } else {
      this.handleSearch()
      this.onSetHistory()
    }
  },
  handleSearch() {
    // 请求数据执行搜索
    const {searchKey} = this.data
    if (searchKey) {
      this.setData({
        goodsList: {
          list: [],
          hasNext: true,
          pageNum: 1,
          pageSize: 10
        },
        listShow: true,
        goodsType: 'search'
      }, () => {
        this.getSearchGoodsList({
          goodsName: searchKey
        })
      })
    } else {
      this.setData({
        listShow: true
      })
    }
  },
  onInputChange({detail: {value}}) {
    // 输入变化
    if (value) {
      this.setData({
        searchKey: value
      })
    }
  },
  onClear() {
    // 清除搜索内容
    this.setData({
      history: []
    }, () => {
      wx.setStorageSync('searchHistory', "")
    })
  },
  onSetSearchText({detail: {text}}) { 
    // 点击搜索词设置搜索内容并搜索
    this.setData({
      searchKey: text
    }, () => {
      this.handleSearch()
      this.onSetHistory()
    })
  },
  onInputFocus() {
    // 聚焦事件,在有列表的情况下聚焦，显示搜索历史
    if (this.data.listShow) {
      this.setData({
        listShow: false
      })
    }
  },


  onChangeShowType() {
    // 切换显示方式
    this.setData({
      squareShow: !this.data.squareShow
    })
  },
  getCateGoodsList() {
    // 从分类列表进入的数据请求方式
    const {catName, catId} = this.options
    const foo = getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: 'result.list',
      url: '/goods/getGoodsListByCatId',
      data: {catName, catId}
    })
    foo()
  },
  getBrandGoodsList() {
    const {bcId} = this.options
    const foo = getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: 'list',
      url: '/brand/queryBrandGoodsListByBcId',
      data: {bcId} 
    })
    foo()
  },
  getSearchGoodsList(data = {}) {
    // 搜索出的列表的情况的请求方式
    const {searchKey} = this.data
    const foo = getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: 'result.list',
      url: '/goods/getGoodsListByGoodsName',
      data: {goodsName: searchKey, ...data}
    })
    foo()
  },
  onPageScroll({scrollTop}) {
    this.setData({
      scrollTop
    })
  },
  bindScrollToTop() {
    wx.pageScrollTo({
      scrollTop: 0
    })
  },
})