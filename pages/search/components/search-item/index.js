
Component({

  properties: {
    text: String
  },

  data: {

  },

  methods: {
    onTap() {
      this.triggerEvent('ontap', {text: this.data.text})
    }
  }
})
