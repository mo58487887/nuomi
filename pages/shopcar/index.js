import ajax from '../../utils/request'
Page({

  data: {
    carList: [],
    selectedGoods: [], // 已选商品,
    editType: false,
    allMoney: 0
  },

  onLoad(options) {

  },

  onShow() {
    ajax({
      url: '/shoppingCar/getCarByUser',
      data: {userId: wx.getStorageSync('userInfo').userId}
    }).then(({code, data}) => {
      if (code === 0 && data && Array.isArray(data.shoppingCar) && data.shoppingCar.length > 0) {
        this.setData({
          carList: data.shoppingCar
        })
      }
    })
    this.setData({
      editType: false
    })
  },
  getAllMoney(all = this.data.carList, sel = this.data.selectedGoods) {
    var selectGoods = all.filter(_ => sel.find(s => s === _.cid))
    var money = 0
    selectGoods.forEach(function(_) {
      money = money + _.goodsPrice * _.goodsNumber
    })
    this.setData({allMoney: money})
  },
  // 修改编辑状态
  onSetEditType() {
    this.setData({
      editType: !this.data.editType
    })
  },
  // 输入修改数量
  onCarNumInput({detail: {value}, currentTarget: {dataset: {cid, idx}}}) {
    const {carList} = this.data
    const item = carList[idx]
    const res = parseInt(value)
    if (value.indexOf('.') > -1) {
      wx.showToast({title: '请输入整数', icon: 'none'})
      this.setData({[`carList[${idx}]`]: Object.assign({}, item, {goodsNumber: item.goodsNumber})})
      return
    }
    if (res <= 0) {
      wx.showToast({
        title: '输入数量不可小于0', icon: 'none'
      })
      this.setData({[`carList[${idx}]`]: Object.assign({}, item, {goodsNumber: item.goodsNumber})})
      return
    } else {
      ajax({
        url: '/shoppingCar/updateShoppingCarNum',
        data: {goodsNumber: res, cId: item.cid}
      }).then(({code, message}) => {
        if (code === 0) {
          this.setData({
            [`carList[${idx}]`]: Object.assign({}, item, {goodsNumber: res})
          })
        } else {
          wx.showToast({title: message, icon: 'none'})
          this.setData({[`carList[${idx}]`]: Object.assign({}, item, {goodsNumber: item.goodsNumber})})
        }
      })
    }
  },
  onLessCarNumber() {

  },
  onMoreCarNumber() {

  },
  // 删除某一购物车项
  onRemoveCarItem({currentTarget: {dataset: {cid}}}) {
    this.handleRemoveCar([cid])
  },
  // 删除所有选中的购物车数据
  onRemoveAllCar() {
    this.handleRemoveCar(this.data.selectedGoods)
  },
  handleRemoveCar(arr) {
    ajax({
      url: '/shoppingCar/deleteBycIds',
      data: {cId: arr.join(',')}
    }).then(({code}) => {
      if (code === 0) {
        console.log(123)
        this.setData({
          carList: this.data.carList.filter(_ => arr.find(a => a === _.cid) ? false : true),
          selectedGoods: this.data.selectedGoods.filter(_ => arr.find(a => a === _) ? false : true)
        }, this.getAllMoney)
        wx.showToast({
          title: '删除成功',
          icon: 'none'
        })
      }
    })
  },
  // 单选
  onSingleGoodsSelect({currentTarget: {dataset: {cid}}}) {
    const {selectedGoods} = this.data
    if (selectedGoods.find(_ => _=== cid)) {
      this.setData({
        selectedGoods: selectedGoods.filter(_ => _ !== cid)
      }, this.getAllMoney)
    } else {
      this.setData({
        selectedGoods: selectedGoods.concat([cid])
      }, this.getAllMoney)
    }
  },
  onSelectAll() {
    const {selectedGoods, carList} = this.data
    if (carList.length === selectedGoods.length) {
      // 取消全选
      this.setData({
        selectedGoods: []
      }, this.getAllMoney)
    } else {
      // 全选
      this.setData({
        selectedGoods: carList.map(_ => _.cid)
      }, this.getAllMoney)
    }
  },
  // 结算
  onSettlement() {    
    const {selectedGoods} = this.data

  },
  onImageError({currentTarget: {dataset: {idx}}}) {
    this.setData({
      [`carList[${idx}].shoppingCarGoodsImg`]: '/asset/error.jpg'
    })
  }
})