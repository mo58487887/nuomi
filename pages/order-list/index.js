const typeNav = [
  {
    type: 0,
    text: '全部'
  },
  {
    type: 1,
    text: '待付款'
  },
  {
    type: 2,
    text: '待发货'
  },
  {
    type: 3,
    text: '待收货'
  },
]
Page({


  data: {
    typeNav,
    currentType: 0
  },

  onShow() {
    const {options: {type}} = this
    if (type) {
      this.setData({
        currentType: parseInt(type)
      })
    }
  },
  onTabChange({detail: {type}}) {
    this.setData({
      currentType: type
    })
  }
})