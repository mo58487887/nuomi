
Page({


  data: {

  },

  onLoad(options) {

  },
  // 输入监控
  onInputChange({detail: {value}, target: {dataset: {type}}}) {
    this.setData({
      [type]: value
    })
  },
  // 失焦
  onBlurTap({currentTarget: {dataset: {type}}}) {
    this.setData({
      [`focus.${type}`]: false
    })
  },
  // 点击聚焦
  onFocusTap({currentTarget: {dataset: {type}}}) {
    this.setData({
      [`focus.${type}`]: true
    })
  },
  // 选择图像
  onChooseImage() {
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success({tempFilePaths}) {
        const cardPath = tempFilePaths[0]

      }
    })
  },
})