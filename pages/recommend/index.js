import { getList } from "../../utils/util";
import ajax from "../../utils/request";

Page({

  data: {
    goodsList: {
      list: [],
      pageNum: 1,
      pageSize: 10,
      hasNext: true
    }
  },
  onLoad (options) {
    ajax({
      url: '/nuomiRecommend/queryRecommendNuomiList',
      data: {page: 1, rows: 10}
      
    })
  },
  getList() {
    const foo = getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: '',
      url: '/nuomiRecommend/queryRecommendNuomiList'
    })
    foo()
  }
})