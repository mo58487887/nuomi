import ajax from "../../utils/request";
Page({

  data: {
    firstCate: [],
    currentFirstCate: 0,
    secondCate: []
  },

  onLoad(options) {
    ajax({
      url: '/goodsCategory/queryNewGoodsCategoryByOneLevel'
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          firstCate: data.list,
          currentFirstCate: data.list[0].cId
        }, this.getSecondCate)
      }
    })
  },
  onTapFirstCate({currentTarget: {dataset: {cId}}}) {
    this.setData({
      currentFirstCate: cId
    }, this.getSecondCate)
  },
  getSecondCate(cId = this.data.currentFirstCate) {
    const {secondCate} = this.data
    const currentCate = secondCate.find(_ => _.cId === cId)
    if (currentCate && currentCate.list.length > 0) {
      return
    }
    ajax({
      url: '/goodsCategory/queryNewGoodsCategoryByCatId',
      data: {catId: cId}
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          secondCate: secondCate.concat([{
            list: data.list,
            cId
          }])
        })
      }
    })
  }
})