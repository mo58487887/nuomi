import { placeImage } from "../../utils/util";

const orderNav = [
  {
    icon: '/asset/daifukuan.png',
    path: '/pages/order-list/index?type=1',
    title: '待付款'
  },
  {
    icon: '/asset/daifahuo.png',
    path: '/pages/order-list/index?type=2',
    title: '待发货'
  },
  {
    icon: '/asset/daishouhuo.png',
    path: '/pages/order-list/index?type=3',
    title: '待收货'
  },
]
const mainNav = [
  {
    icon: '/asset/youhuiquan.png',
    path: '/pages/coupon/index',
    title: '优惠券'
  },
  {
    icon: '/asset/shouhuodizhi.png',
    path: '/pages/address/index',
    title: '收货地址'
  },
  {
    icon: '/asset/shimingrenzheng.png',
    path: '/pages/real-name/index',
    title: '实名认证'
  },
  {
    icon: '/asset/lianxikefu.png',
    path: '',
    title: '联系客服'
  },
]

const app = getApp()
Page({

  data: {
    orderNav,
    mainNav,
    showView: 0,
    userInfo: {}
  },
  onLoad() {
    if (app.globalData.userInfo) {
      this.setData({
        showView: 1
      })
    } else {
      this.setData({
        showView: -1
      })
    }
  },
  onSuccessLogin({detail: {data}}) {
    this.setData({
      showView: 1,
      userInfo: data
    })
  }
})