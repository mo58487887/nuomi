import ajax from "../../utils/request";
import { getList } from "../../utils/util";

Page({

  data: {
    killGoods: {
      pageNum: 1,
      pageSize: 10,
      list: [],
      hasNext: true,
    }
  },

  onShow() {
    this.setData({
      hasNext: true
    }, () => {
      this.getList(1, 10)
    })
  },
  getList() {
    const foo = getList.bind(this, {
      localKeyName: 'killGoods',
      dataKeyName: 'limitGoods',
      url: '/index/getLimitGoodsListByNowDate'
    })
    foo()
  },

  onReachBottom	() {
    this.getList()
  }
})