import ajax from "../../utils/request";

Page({

  data: {
    consigneeName: '',
    consigneeMoblie: '',
    consigneeAddress: '',
    consigneeState: 1,
    consigneeZip: '',
    focus: {},
    province: '',
    provinceList: [],
    city: '',
    cityList: [],
    area: '',
    areaList: [],
  },

  onLoad(options) {
    this.getRegionList()
  },
  onOpenPickerView() {
    this.setData({
      currentRegionChoose: 1
    })
  },
  // 获取省市区列表
  getRegionList(parentId = 1, type = 'provinceList', callback) {
    ajax({
      url: '/region/getcity',
      data: {parentId}
    }).then(({code, data}) => {
      if (code === 0) {
        this.setData({
          [type]: data.result
        })
        if (callback) callback(data)
      }
    })
  },
  onRegionChange({currentTarget: {dataset: {type}}, detail: {value}}) {
    const {provinceList, cityList, areaList} = this.data
    if (type === 'province') {
      this.setData({
        cityList: [],
        city: '',
        areaList: [],
        area: ''
      })
      this.getRegionList(provinceList[value]['region_id'], 'cityList')
    } else if (type === 'city') {
      this.setData({
        areaList: [],
        area: ''
      })
      this.getRegionList(cityList[value]['region_id'], 'areaList')
    } else {

    }
    this.setData({
      [type]: value
    })
  },
  initRegionData() {
    this.getRegionList(1, 0, data => {
      this.getRegionList(data.result[0]['region_id'], 1, data => {
        this.getRegionList(data.result[0]['region_id'], 2)
      })
    })
  },
  onRegionEmptyTap() {
    wx.showToast({
      title: '请先选择上级区域',
      icon: 'none'
    })
  },
  // 输入监控
  onInputChange({detail: {value}, target: {dataset: {type}}}) {
    this.setData({
      [type]: value
    })
  },
  // 失焦
  onBlurFocus({currentTarget: {dataset: {type}}}) {
    this.setData({
      [`focus.${type}`]: false
    })
  },
  // 点击聚焦
  onFocusTap({currentTarget: {dataset: {type}}}) {
    this.setData({
      [`focus.${type}`]: true
    })
  },
  onOpenPickerView() {
    this.setData({
      showRegionPicker: true
    })
  },
  // 默认切换
  onSetConsigneeState() {
    this.setData({
      consigneeState: this.data.consigneeState === 0 ? 1 : 0
    })
  },
  onSaveTap() {
    const {addressId} = this.options
    const {consigneeName, consigneeMoblie, consigneeAddress, consigneeState, consigneeZip, province, provinceList, city, cityList, area, areaList} = this.data
    const sendData = {
      consigneeName, consigneeMoblie, consigneeAddress, consigneeState, consigneeZip,
      consigneeProvinceId: provinceList[province]['region_id'],
      consigneeCityId: cityList[city]['region_id'],
      consigneeAreaId: areaList[area]['region_id'],
      userId: wx.getStorageSync('userInfo').userId,
    }
    if (addressId) sendData.addressId = addressId
    ajax({
      url: addressId ? '/consignee/updateConsignee' : '/consignee/addConsignee',
      data: {data: JSON.stringify(sendData)}
    }).then(({code, data}) => {
      if (code === 0) {
        wx.showToast({
          title: '编辑成功',
          icon: 'none'
        })
        wx.navigateBack({
          delta: 1
        })
      }
    })
  }
})