import { getList } from "../../utils/util";

Page({

  data: {
    goodsList: {
      list: [],
      hasNext: true,
      pageNum: 1,
      pageSize: 20
    }
  },

  onShow(options) {
    this.getList()
  },
  getList() {
    getList.bind(this, {
      localKeyName: 'goodsList',
      dataKeyName: 'list',
      url: '/seasonPush/querySeasonPushGoodsList'
    })()
  }
})