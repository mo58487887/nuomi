import ajax from "../../utils/request";
import { getList } from "../../utils/util";

Page({

  data: {
    kill: [],
    currentKillGoods: {
      list: [],
      pageNum: 1,
      pageSize: 10,
      hasNext: true
    },
    currentKillTime: ''
  },

  onLoad(options) {

  },
  onTimeTap({currentTarget: {dataset: {queryTime}}}) {
    this.setData({
      'currentKillGoods': {
        list: [],
        pageNum: 1,
        pageSize: 10,
        hasNext: true
      }
    }, () => {
      this.getGoods(queryTime, () => {
        this.setData({
          currentKillTime: queryTime
        })
      })
    })
  },
  onShow() {
    ajax({
      url: '/secondsKill/queryKillList'
    }).then(({code, data}) => {
      if (code === 0 && data && data.kill) {
        const currentKillTime = data.kill.find(_ => _.code == '2') && data.kill.find(_ => _.code == '2').queryTime
        this.setData({
          kill: data.kill,
          currentKillTime: currentKillTime || ''
        })
        if (currentKillTime) {
          return currentKillTime
        } else {
          return data.kill[0].queryTime
        }
      }
    }).then(res => {
      this.getGoods(res)
    })
  },
  getGoods(queryTime = this.data.currentKillTime, callback) {
    getList.call(this, {
      localKeyName: 'currentKillGoods',
      dataKeyName: 'list',
      url: '/secondsKill/queryKillGoodsByNowDate',
      data: {nowDate: queryTime},
      callback
    })
  },
  onReachBottom() {
    this.getGoods()
  }
})